\select@language {slovene}
\contentsline {chapter}{\numberline {1}Uvod}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Navedba realiziranih storitev}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Spletna Trgovina}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Frontend}{3}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}REST API}{3}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Vloge}{4}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}EDIT ME}{4}{subsection.2.1.4}
\contentsline {section}{\numberline {2.2}Android aplikacija}{4}{section.2.2}
\contentsline {chapter}{\numberline {3}Podatkovni model}{5}{chapter.3}
\contentsline {chapter}{\numberline {4}Varnost sistema}{6}{chapter.4}
\contentsline {chapter}{\numberline {5}Izjava o avtorstvu seminarske naloge}{7}{chapter.5}
\contentsline {chapter}{\numberline {6}Dodatno vzor\IeC {\v c}no poglavje}{10}{chapter.6}
\contentsline {section}{\numberline {6.1}Dodatni vzor\IeC {\v c}ni odsek ena}{10}{section.6.1}
\contentsline {section}{\numberline {6.2}Dodatni vzor\IeC {\v c}ni odsek dva}{10}{section.6.2}
\contentsline {section}{\numberline {6.3}Dodatni vzor\IeC {\v c}ni odsek tri}{10}{section.6.3}
\contentsline {chapter}{\numberline {7}Zaklju\IeC {\v c}ek}{12}{chapter.7}
\contentsline {chapter}{\numberline {8}Literatura}{13}{chapter*.2}
\vspace {15pt}
\contentsline {chapter}{\numberline {A}Naslov dodatka}{14}{appendix.A}
