#!/bin/bash

BASE_DIR=~/Desktop/ep-seminarska-naloga
SERVER_DIR=/var/www/ep-store
APACHE_DIR=/etc/apache2

TMP_DIR=$BASE_DIR/tmp
STUB_DIR=$BASE_DIR/stubs

# Remove symlink from server folder
rm -f $SERVER_DIR