#!/bin/bash


BASE_DIR=~/Desktop/ep-seminarska-naloga
SERVER_DIR=/var/www/ep-store
APACHE_DIR=/etc/apache2

TMP_DIR=$BASE_DIR/tmp
STUB_DIR=$BASE_DIR/install/stubs

SQL_USER=root
SQL_PASS=ep
DB_NAME=fri_ep

bold=`tput bold`
normal=`tput sgr0`

printf "\n"

# Make sure to run as root, otherwise permission errors will be plenty
if [ `id -u` -ne 0 ]; then
    printf "${bold}Please run this as root${normal}\n\n"
    exit 1
fi

# EXECUTABLE CHECKS
errors=()
type php &>/dev/null || errors+=('php')
type git &>/dev/null || errors+=('git')
type mysql &>/dev/null || errors+=('mysql')
type curl &>/dev/null || errors+=('curl')
type rsync &>/dev/null || errors+=('rsync')

if [ ${#errors[@]} -gt 0 ]; then
    printf "${bold}The installation script requires the following programs to be installed:${normal}\n"
    for missing in "${errors[@]}"; do
        printf "    $missing\n"
    done
    printf "\n"
    exit 1
fi

# CREATE AND CLEAR DIRECTORIES
[ -d $TMP_DIR ] && rm -rf $TMP_DIR
mkdir $TMP_DIR

# SET UP LARAVEL SERVER
printf "${bold}Running installation script${normal}\n\n"
# Move server into webserver folder
[ -e $SERVER_DIR ] && rm $SERVER_DIR
ln -s $BASE_DIR/php $SERVER_DIR

# Fetch composer
printf "  -> Fetching composer\n"
curl -sS https://getcomposer.org/installer | php &> /dev/null
mv composer.phar $TMP_DIR/

# Composer only works in current directory
printf "  -> Installing server dependencies\n"
cd $SERVER_DIR
$TMP_DIR/composer.phar install &> /dev/null

# TODO run laravel migrations and setup database

# CONFIGURE APACHE2
cp $STUB_DIR/apache/sites-available/* $APACHE_DIR/sites-available/
a2ensite ep-store.local.conf &> /dev/null
a2enmod rewrite &> /dev/null
a2enmod ssl &> /dev/null

[ ! -d $APACHE_DIR/ssl ] && mkdir $APACHE_DIR/ssl
cp $STUB_DIR/apache/ssl/* $APACHE_DIR/ssl/

# CONFIGURE HOSTS FILE
printf "  -> Updating hosts file\n"
if ! grep -q ep-store.local /etc/hosts; then
    cat <<EOF >> /etc/hosts
127.0.0.1   ep-store.local
127.0.0.1   api.ep-store.local
EOF
fi

# CONFIGURE MYSQL
printf "  -> Creating database $DB_NAME\n"
mysql --user="$SQL_USER" --password="$SQL_PASS" --execute="DROP DATABASE IF EXISTS $DB_NAME; CREATE DATABASE $DB_NAME" &> /dev/null
printf "  -> Running migrations and seeding tables\n"

# CONFIGURE LARAVEL
cd $SERVER_DIR
# Populate the database
php artisan migrate --seed &> /dev/null7

# CONFIGURE PERMISSIONS
chown -R ep $BASE_DIR
chmod -R o+w $SERVER_DIR/storage/framework/
chmod -R o+w $SERVER_DIR/public


printf "\n${bold}Server configuration complete${normal}\n"
printf "\n${bold}Please restart the apache server to complete installation${normal}\n"

printf "\n"
